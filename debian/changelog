grdesktop (0.23+d040330-8) unstable; urgency=medium

  * QA upload.

  * Updated vcs in d/control to Salsa.
  * Added d/gbp.conf to enforce the use of pristine-tar.
  * Updated Standards-Version from 4.5.0 to 4.7.0.
  * Replaced obsolete pkg-config build depend with pkgconf.  Thanks lintian.

 -- Petter Reinholdtsen <pere@debian.org>  Tue, 16 Apr 2024 08:11:03 +0200

grdesktop (0.23+d040330-7) unstable; urgency=medium

  * QA upload.
  * debian/patches/11_gcc-10.diff: New; fix FTBFS with GCC 10
    (Closes: #957308).
  * debian/patches/series: Update.
  * debian/control (Build-Depends): Switch to debhelper-compat; bump
    compat level to 13.
  * debian/compat: Delete.

 -- Yavor Doganov <yavor@gnu.org>  Tue, 23 Jun 2020 12:12:21 +0300

grdesktop (0.23+d040330-6) unstable; urgency=medium

  * QA upload.
  * debian/patches/08_gsettings-port.diff: Set default keymap; thanks
    Frank Doepper (Closes: #952636).
  * debian/control (Standards-Version): Bump to 4.5.0; no changes needed.

 -- Yavor Doganov <yavor@gnu.org>  Sat, 21 Mar 2020 16:07:13 +0200

grdesktop (0.23+d040330-5) unstable; urgency=medium

  * QA upload.
  * debian/patches/08_gsettings-port.diff: Fix regression in handling of
    the RDP protocol setting (Closes: #945842).  Thanks Thomas Hooge.

 -- Yavor Doganov <yavor@gnu.org>  Fri, 29 Nov 2019 19:21:44 +0200

grdesktop (0.23+d040330-4) unstable; urgency=medium

  * QA upload.
  * debian/patches/08_gsettings-port.diff: Remove GConf migration code.
  * debian/patches/09_gtk3-port.diff: New, port to GTK 3.  Incidentally,
    this fixes incorrect scrolling in the Resources tab (LP: #1024156).
  * debian/patches/10_glib-deprecations.diff: New, replace some deprecated
    GLib functions that trigger compiler warnings.
  * debian/patches/03_desktop.diff: Remove Encoding, add Keywords entry.
    Use absolute path for the icon.
  * debian/patches/04_typos.diff: Fix more typos.
  * debian/patches/06_no-libgnome.diff: Refresh.
  * debian/patches/01_gettext.diff: Add description.
  * debian/patches/05_format-security.diff: Likewise.
  * debian/patches/series: Update.
  * debian/compat: Bump to 12.
  * debian/control: Run wrap-and-sort -ast.
    (Maintainer): Set to the Debian QA Group; package is orphaned.
    (Rules-Requires-Root): Set to no.
    (Build-Depends): Replace libgtk2.0-dev with libgtk-3-dev.  Bump
    debhelper requirement to match the compat level.
    (Depends): Remove ancient rdesktop version requirement.
    (Recommends): Remove gconf2.
    (Standards-Version): Claim compliance with 4.4.1 as of this release.
  * debian/rules: Enable hardening.  Remove trailing whitespace.
    (override_dh_autoreconf): New; fixes building twice in a row.
    (override_dh_clean): Also delete doc/Makefile; upstream overrides the
    clean/distclean targets.
  * debian/menu: Delete.
  * debian/docs: Remove AUTHORS, README, NEWS and FAQ.html; not useful.
  * debian/links: Delete; AppStream cannot cope with symlinks.
  * debian/watch: Switch to version 4; use Savannah's mirror multiplexor.
  * debian/copyright: Rewrite as machine-readable.
  * debian/changelog: Remove empty line.

 -- Yavor Doganov <yavor@gnu.org>  Sun, 20 Oct 2019 12:50:18 +0300

grdesktop (0.23+d040330-3.1) unstable; urgency=medium

  * Non-maintainer upload

  [ Yavor Doganov ]
  * Add 06_no-libgnome.diff:
    - Avoid libgnome dependency (Closes: #868427)
  * Add 07_avoid-scrollkeeper.diff:
    - Stop using rarian (Closes: #885115)
  * Add 08_gsettings-port.diff:
    - Port from gconf to gsettings. Recommend gconf2 so that user
      settings can be converted (Closes: #886071)

  [ Jeremy Bicha ]
  * Bump debhelper compat to 11
  * Update dependencies

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 15 Nov 2018 15:04:26 -0500

grdesktop (0.23+d040330-3) unstable; urgency=low

  * debian/rules: Enabled buildflags.
  * debian/patches/05_format-security.diff: Closes: #646269.
  * debian/control: Fixed missing-build-dependency dpkg-dev (>= 1.16.1~).

 -- Bart Martens <bartm@debian.org>  Sun, 13 May 2012 06:25:09 +0000

grdesktop (0.23+d040330-2) unstable; urgency=low

  * Switch to dpkg-source 3.0 (quilt) format.
  * debian/control: Use Homepage field.  Closes: #615351.
  * debian/patches/04_typos.diff: Added.  Closes: #566015.

 -- Bart Martens <bartm@debian.org>  Mon, 29 Aug 2011 20:48:30 +0200

grdesktop (0.23+d040330-1) unstable; urgency=low

  * New maintainer.  Closes: 398120.
  * debian/copyright: Updated.
  * debian/menu: Fixed lintian warnings.
  * debian/patches/02_iter.diff: Added.  Closes: #390123.
  * debian/rules: Removed use of tarball.mk, added use of autotools.mk.
  * debian/patches/03_desktop.diff: Added to fix lintian warnings.
  * debian/watch: Added.

 -- Bart Martens <bartm@debian.org>  Sun, 25 Nov 2007 20:47:16 +0100

grdesktop (0.23-3) unstable; urgency=low

  * QA upload. (ACK NMU; Closes: #320464)
  * Set maintainer to QA Group; Orphaned: #398120
  * Add gconf2 (>= 2.10.1-2) to Dependencies as the
    maintainer scripts call gconf-schemas.
  * Conforms with latest Standards Version 3.7.2

 -- Michael Ablassmeier <abi@debian.org>  Fri, 24 Nov 2006 14:47:17 +0100

grdesktop (0.23-2) unstable; urgency=low

  * Create symblic link from grdesktop/icon.png to grdesktop.png.
    (Closes: #257427)
  * debian/control: depend on newer debconf version (needed by cdbs).
  * Debian-Patches:
    - rename all @GETTEXT_PACKAGE@.mo files to grdesktop.mo

 -- Thorsten Sauter <tsauter@debian.org>  Mon, 19 Jul 2004 16:43:48 +0200

grdesktop (0.23-1) unstable; urgency=low

  * New upstream version.
    - remove invalid character in Makefile.am (Closes: #231737)
    - NULL pointer problem fixed by upstream. (Closes: #231309)
  * debian/control: remove docbook-to-man dependancy
  * debian/menu: make the menu file policy conform

 -- Thorsten Sauter <tsauter@debian.org>  Tue, 30 Mar 2004 16:47:24 +0200

grdesktop (0.22-1) unstable; urgency=low

  * New upstream bugfix version.

 -- Thorsten Sauter <tsauter@debian.org>  Tue,  3 Feb 2004 11:56:41 +0100

grdesktop (0.21-1) unstable; urgency=low

  * New upstream version.

 -- Thorsten Sauter <tsauter@debian.org>  Tue,  3 Feb 2004 10:12:57 +0100

grdesktop (0.20-2) unstable; urgency=low

  * debian/control: depend on scrollkepper. (Closes: #228657)

 -- Thorsten Sauter <tsauter@debian.org>  Tue, 20 Jan 2004 11:39:23 +0100

grdesktop (0.20-1) unstable; urgency=low

  * New upstream version.
  * Fixed in new upstream version: Closes: #219413
    - grdesktop allow the user to select the rdp protcol
    - new upstream version contains most of the new rdesktop features.
  * Include FAQ from grdesktop homepage
  * Update package description
  * Switch to cdbs for package building

 -- Thorsten Sauter <tsauter@debian.org>  Fri,  9 Nov 2003 13:45:52 +0100

grdesktop (0.19-3) unstable; urgency=low

  * Update standards version.

 -- Thorsten Sauter <tsauter@debian.org>  Wed, 22 Oct 2003 22:43:40 +0200

grdesktop (0.19-2) unstable; urgency=low

  * debian/control:
    - update standards version
    - change my email address
  * debian/rules: move dh_compat to debian/compat

 -- Thorsten Sauter <tsauter@debian.org>  Sat, 31 May 2003 10:38:24 +0200

grdesktop (0.19-1) unstable; urgency=low

  * New upstream version.

 -- Thorsten Sauter <tsauter@gmx.net>  Thu, 27 Feb 2003 11:59:33 +0100

grdesktop (0.18-2) unstable; urgency=low

  * Fix infinite loop problem on some archs. (Closes: #179484)

 -- Thorsten Sauter <tsauter@gmx.net>  Thu,  6 Feb 2003 21:55:41 +0100

grdesktop (0.18-1) unstable; urgency=low

  * New upstream version. (Closes: #176605)

 -- Thorsten Sauter <tsauter@gmx.net>  Sat, 25 Jan 2003 00:06:12 +0100

grdesktop (0.17-2) unstable; urgency=low

  * Change keymaps path to be compatible with rdesktop (Closes: #175171)
  * debian/docs: remove readme file

 -- Thorsten Sauter <tsauter@gmx.net>  Fri,  3 Jan 2003 18:39:19 +0100

grdesktop (0.17-1) unstable; urgency=low

  * New upstream version.
  * Reupload after death for satie :)
  * debian/control: change section (rdesktop is now in main)
  * debian/copyright: Fix copyright informations
  * debian/rules: upgrade standards version

 -- Thorsten Sauter <tsauter@gmx.net>  Thu, 21 Nov 2002 22:53:47 +0100

grdesktop (0.14-2) unstable; urgency=low

  * debian/dirs: insert directories
  * debian/rules: - remove unused configure options
  		  - remove config.* directives in clean target
		  - insert DEB_BUILD_OPTIONS
		  - remove all created man-pages in after distclean
  * debian/control: - change description line
                    - change section to non-US (because rdesktop)
  * First upload to debian directory. Close ITP. Closes: #164025

 -- Thorsten Sauter <tsauter@gmx.net>  Wed, 16 Oct 2002 21:00:32 +0200

grdesktop (0.14-1) unstable; urgency=low

  * New upstream version.

 -- Thorsten Sauter <tsauter@gmx.net>  Tue, 15 Oct 2002 23:32:13 +0200

grdesktop (0.12-2) unstable; urgency=low

  * debian/README.Debian: remove
  * debian/rules: remove docbook-to-man command

 -- Thorsten Sauter <tsauter@gmx.net>  Sun, 13 Oct 2002 01:03:51 +0200

grdesktop (0.12-1) unstable; urgency=low

  * New upstream version
  * debian/menu: change icon name to xpm format

 -- Thorsten Sauter <tsauter@gmx.net>  Sat, 12 Oct 2002 13:32:39 +0200

grdesktop (0.10-2) unstable; urgency=low

  * debian/control: fix spelling errors.

 -- Thorsten Sauter <tsauter@gmx.net>  Thu, 10 Oct 2002 20:33:53 +0200

grdesktop (0.10-1) unstable; urgency=low

  * New upstream version.

 -- Thorsten Sauter <tsauter@gmx.net>  Thu, 10 Oct 2002 00:06:03 +0200

grdesktop (0.01-1) unstable; urgency=low

  * Initial Release.

 -- Thorsten Sauter <tsauter@gmx.net>  Mon,  7 Oct 2002 23:51:51 +0200
